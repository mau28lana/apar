using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject fireToDestroy;
    public GameObject objectToSpawn;
    public AudioSource AudioSource;
    public Transform spanwedObjectTransform = null;
    SpawnObject SpawnObject;

    [SerializeField, Range(0f, 1f)] private float currentItensity = 1.0f;
    
    private float[] startIntensities = new float[0];

    [SerializeField] private ParticleSystem [] fireParticleSystems = new ParticleSystem[0];

    private void Start()
    {
        
        startIntensities = new float[fireParticleSystems.Length];

        for (int i = 0; i < fireParticleSystems.Length; i++)
        {
            startIntensities[i] = fireParticleSystems[i].emission.rateOverTime.constant;

        }
    }

    
    private void Update()
    {
        // ChangeIntensity();
    }

    public void Play()
    {
        AudioSource.Play();
    }

    public void Stop()
    {
        AudioSource.Stop();
    }

    public bool TryExtinguish(float amount)
    {
        currentItensity -= amount;
        ChangeIntensity();

        if(currentItensity < 0)
        {
            Destroy(fireToDestroy);
            Instantiate(objectToSpawn, spanwedObjectTransform.position, spanwedObjectTransform.rotation);
        }
        return currentItensity <= 0;
    }

    private void ChangeIntensity()
    {
        for (int i = 0; i < fireParticleSystems.Length; i++)
        {
            var emission = fireParticleSystems[i].emission;
            emission.rateOverTime = currentItensity * startIntensities[i];
        }
        
    }
}
